import { Component, OnInit, ViewChild } from '@angular/core';
import { TodosService } from './services/todos.service';
import { first } from 'rxjs/operators';
import { Todo } from './models/todo.model';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit{
  title = 'pwa-workshop';
  todos: Todo[];
  todo: Todo = new Todo();

  @ViewChild('f', {static: false}) todoForm: NgForm;

  constructor(private todosService: TodosService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getTodos();
    if ((navigator as any).standalone == false) {
      // This is ios device in the browser
      this.snackBar.open("You can add this PWA to the Home Screen", "", {
        duration: 3000
      });
    }

    if ((navigator as any).standalone == undefined) {
      // It's not ios
      if (window.matchMedia("display-mode: browser").matches) {
        // We are in the browser
        window.addEventListener('beforeinstallprompt', event => {
          event.preventDefault();
          const sb = this.snackBar.open('Do you want to install this app?', "Install", {
            duration: 3000
          });
          sb.onAction().subscribe( () => {
            (event as any).prompt();
            (event as any).userChoice.then( result => {
              if (result.outcome == 'dismissed') {
                // TODO: Track no installation
              } else {
                // TODO: It was installed
              }
            })
          })
          return false;
        })
      }
    }
  }

  createTodo(): void {
    this.todosService.createTodo(this.todo)
      .pipe(first())
      .subscribe(newTodo => {
        this.getTodos();
        this.todoForm.form.reset();
      });
  }

  completeTodo(todo: Todo): void {
    this.todosService.deleteTodo(todo)
      .pipe(first())
      .subscribe(() => {
        console.log('Todo was deleted!');
        this.getTodos();
      })
  }

  getTodos(): void {
    this.todosService.getTodos()
      .pipe(first())
      .subscribe(todos => {
        this.todos = todos;
      })
  }
}
