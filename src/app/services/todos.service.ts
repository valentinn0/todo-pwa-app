import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Todo } from '../models/todo.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  constructor(private httpClient: HttpClient) {}

  endPoint = "http://192.168.245.206:3000";

  getTodos(): Observable<Todo[]> {
    return this.httpClient.get<Todo[]>(this.endPoint + '/todos');
  }

  createTodo(todo: Todo): Observable<Todo> {
    return this.httpClient.post<Todo>(this.endPoint + '/todos', todo)
  }

  deleteTodo(todo: Todo): Observable<Todo> {
    return this.httpClient.delete<Todo>(this.endPoint + '/todos/' + todo._id);
  }
}
